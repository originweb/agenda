var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('default', function(){
    gulp.src('sass/style.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css/'));
});

gulp.task('watch',function() {
    gulp.watch('sass/style.sass', ['default']);
});